using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using pirelimap1.Data;

namespace pirelimap1.Controllers
{


    //[Produces("application/json")]
    
    public class MapController : Controller
    {
        private readonly TireContext _context;

        public MapController(TireContext context)
        {
            _context = context;
        }

        [Route("api/Map")]
        [HttpGet]
        public IActionResult Index()
        {
            
            return View();
        }

        [Route("api/Map/MapAddresses")]
        [HttpGet]
        public IActionResult MapAddresses()
        {
            var lst = _context.Tires.Select(x => x.Adress).ToList();
            return Json(lst);
        }
        [Route("api/Map/MapTiresNames")]
        [HttpGet]
        public IActionResult MapTiresNames()
        {
            var TireName = _context.Tires.Select(x => x.NameTire).ToList();
            return Json(TireName);
        }


    }
}