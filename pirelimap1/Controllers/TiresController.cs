using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using pirelimap1.Data;
using pirelimap1.Models;

namespace pirelimap1.Controllers
{
    public class TiresController : Controller
    {
        private readonly TireContext _context;

        public TiresController(TireContext context)
        {
            _context = context;    
        }

        // GET: Tires
        public async Task<IActionResult> Index(string sortOrder, string searchString)
        {
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";
            ViewData["CurrentFilter"] = searchString;
            var tires = from s in _context.Tires
                           select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                tires = tires.Where(s => s.NameTire.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    tires = tires.OrderBy(s => s.NameTire);
                    break;
                case "Date":
                    tires = tires.OrderBy(s => s.Adress);
                    break;
                case "date_desc":
                    tires = tires.OrderByDescending(s => s.Zipcode);
                    break;
               
            }
            return View(await tires.AsNoTracking().ToListAsync());
        }

        // GET: Tires/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tire = await _context.Tires
                .SingleOrDefaultAsync(m => m.ID == id);
            if (tire == null)
            {
                return NotFound();
            }

            return View(tire);
        }

        // GET: Tires/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Tires/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,NameTire,Adress,Zipcode")] Tire tire)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(tire);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            catch (DbUpdateException /* ex */)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }
        
            return View(tire);
        }

        // GET: Tires/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tire = await _context.Tires.SingleOrDefaultAsync(m => m.ID == id);
            if (tire == null)
            {
                return NotFound();
            }
            return View(tire);
        }

        // POST: Tires/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,NameTire,Adress,Zipcode")] Tire tire)
        {
            if (id != tire.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tire);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TireExists(tire.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(tire);
        }

        // GET: Tires/Delete/5
        public async Task<IActionResult> Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tire = await _context.Tires
                 .AsNoTracking()
                .SingleOrDefaultAsync(m => m.ID == id);
            if (tire == null)
            {
                return NotFound();
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewData["ErrorMessage"] =
                    "Delete failed. Try again, and if the problem persists " +
                    "see your system administrator.";
            }

            return View(tire);
        }

        // POST: Tires/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tire = await _context.Tires.SingleOrDefaultAsync(m => m.ID == id);
            _context.Tires.Remove(tire);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool TireExists(int id)
        {
            return _context.Tires.Any(e => e.ID == id);
        }
    }
}
