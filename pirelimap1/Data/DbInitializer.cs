﻿using pirelimap1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pirelimap1.Data
{
    public static class DbInitializer
    {
        public static void Initialize(TireContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Tires.Any())
            {
                return;   // DB has been seeded
            }

            var Tires = new Tire[]
            {
            new Tire{NameTire="Carson",Adress="Alexander",Zipcode=12135},
            //new Tire{FirstMidName="Meredith",LastName="Alonso",EnrollmentDate=DateTime.Parse("2002-09-01")},
            //new Tire{FirstMidName="Arturo",LastName="Anand",EnrollmentDate=DateTime.Parse("2003-09-01")},
            //new Tire{FirstMidName="Gytis",LastName="Barzdukas",EnrollmentDate=DateTime.Parse("2002-09-01")},
            //new Tire{FirstMidName="Yan",LastName="Li",EnrollmentDate=DateTime.Parse("2002-09-01")},
            //new Tire{FirstMidName="Peggy",LastName="Justice",EnrollmentDate=DateTime.Parse("2001-09-01")},
            //new Tire{FirstMidName="Laura",LastName="Norman",EnrollmentDate=DateTime.Parse("2003-09-01")},
            //new Tire{FirstMidName="Nino",LastName="Olivetto",EnrollmentDate=DateTime.Parse("2005-09-01")}
            };
            foreach (Tire s in Tires)
            {
                context.Tires.Add(s);
            }
            context.SaveChanges();
        }
    }
}
