﻿using Microsoft.EntityFrameworkCore;
using pirelimap1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pirelimap1.Data
{
    public class TireContext : DbContext
    {
        public TireContext(DbContextOptions<TireContext> options) : base(options)
        { }

        public DbSet<Tire> Tires { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tire>().ToTable("Tire");
        }
    }
}
