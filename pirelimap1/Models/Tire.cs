﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace pirelimap1.Models
{
    public class Tire
    {
        public int ID { get; set; }
        [Required]
        public string NameTire { get; set; }
        public string Adress { get; set; }
        public int Zipcode { get; set; }
        
    }
}
